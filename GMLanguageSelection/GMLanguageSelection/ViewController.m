//
//  ViewController.m
//  GMLanguageSelection
//
//  Created by Gaston Montes on 14/08/2019.
//  Copyright © 2019 Gaston Montes. All rights reserved.
//

#import "ViewController.h"

#import "NSString+Localization.h"
#import "NSBundleLocalized.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - Alert functions.
- (void)presentLanguageAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[@"ViewController.Alert.Title.Text" stringLocalized]
                                                                             message:[@"ViewController.Alert.Message.Text" stringLocalized]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *closeAction = [UIAlertAction actionWithTitle:[@"ViewController.Alert.Action.Close.Title.Text" stringLocalized]
                                                          style:UIAlertActionStyleCancel
                                                        handler:nil];
    
    [alertController addAction:closeAction];
    
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}

#pragma mark - Actions functions.
- (IBAction)englishButtonAction:(id)sender {
    [[NSBundleLocalized sharedInstance] setNewLanguage:@"en"];
    
    [self presentLanguageAlert];
}

- (IBAction)spanishButtonAction:(id)sender {
    [[NSBundleLocalized sharedInstance] setNewLanguage:@"es"];
    
    [self presentLanguageAlert];
}

@end
