//
//  NSBundleLocalized.h
//  GMLanguageSelection
//
//  Created by Gaston Montes on 14/08/2019.
//  Copyright © 2019 Gaston Montes. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSBundleLocalized : NSObject

+ (NSBundleLocalized *)sharedInstance;

@property (readonly, nonatomic, strong) NSBundle *mainBundle;

- (void)setNewLanguage:(NSString *)language;

@end

NS_ASSUME_NONNULL_END
