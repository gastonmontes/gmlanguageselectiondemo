//
//  NSBundleLocalized.m
//  GMLanguageSelection
//
//  Created by Gaston Montes on 14/08/2019.
//  Copyright © 2019 Gaston Montes. All rights reserved.
//

#import "NSBundleLocalized.h"

@interface NSBundleLocalized ()

@property (nonatomic, strong) NSBundle *mainBundle;

@end

@implementation NSBundleLocalized

#pragma mark - Shared instance creation.
+ (NSBundleLocalized *)sharedInstance {
    static NSBundleLocalized *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NSBundleLocalized alloc] init];
    });
    
    return sharedInstance;
}

#pragma mark - Initialization.
- (NSBundleLocalized *)init {
    if (self = [super init]) {
        _mainBundle = [NSBundle mainBundle];
    }
    
    return self;
}

#pragma mark - Setters
- (void)setNewLanguage:(NSString *)language {
    self.mainBundle = [NSBundle mainBundle];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
    NSBundle *localizedBundle = [NSBundle bundleWithPath:path];
    
    if (localizedBundle != nil) {
        self.mainBundle = localizedBundle;
    } 
}

@end
