//
//  NSString+Localization.h
//  GMLanguageSelection
//
//  Created by Gaston Montes on 14/08/2019.
//  Copyright © 2019 Gaston Montes. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Localization)

- (NSString *)stringLocalized;

@end

NS_ASSUME_NONNULL_END
