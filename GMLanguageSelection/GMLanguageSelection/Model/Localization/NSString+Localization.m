//
//  NSString+Localization.m
//  GMLanguageSelection
//
//  Created by Gaston Montes on 14/08/2019.
//  Copyright © 2019 Gaston Montes. All rights reserved.
//

#import "NSString+Localization.h"

#import "NSBundleLocalized.h"

@implementation NSString (Localization)

- (NSString *)stringLocalized {
    NSString *targetName = [NSBundle.mainBundle.infoDictionary objectForKey:@"CFBundleName"];
    NSString *targetString = NSLocalizedStringFromTableInBundle(self, targetName, [NSBundleLocalized sharedInstance].mainBundle, nil);
    
    if ([targetString isEqualToString:self]) {
        return NSLocalizedStringFromTableInBundle(self, nil, [NSBundleLocalized sharedInstance].mainBundle, nil);
    }
    
    return targetString;
}

@end
